package com.gitlab.candicey.astral.extension

import com.gitlab.candicey.zenithcore.util.GREY
import com.gitlab.candicey.zenithcore.util.LIGHT_PURPLE
import com.gitlab.candicey.zenithcore.util.YELLOW

fun String.addATPrefix() = "$GREY[${LIGHT_PURPLE}AT$GREY]$YELLOW $this"