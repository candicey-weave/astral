package com.gitlab.candicey.astral.config

data class BungeeHackJson(
    var enabled: Boolean = false,
    var ip: String = "127.0.0.1",
)
