package com.gitlab.candicey.astral.config

data class SessionJson(
    var usePremiumUuid: Boolean = false,
    var premiumUuid: String? = null,
    var nick: String? = null,
    var fakeNick: String? = null,
) {
}