package com.gitlab.candicey.astral.config

data class RainbowSpamJson(
    var command: String = "",
    var rainbow: String = "",
    var delay: Int = 250,
)