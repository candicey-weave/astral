package com.gitlab.candicey.astral.config

data class GameOverlayJson(
    var enabled: Boolean = true,
    var hideIp: Boolean = false,
)