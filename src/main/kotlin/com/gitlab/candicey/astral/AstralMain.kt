package com.gitlab.candicey.astral

import com.gitlab.candicey.astral.command.*
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.astral.hook.GuiMultiplayerHook
import com.gitlab.candicey.astral.hook.MinecraftHook
import com.gitlab.candicey.astral.hook.NetHandlerPlayClientHook
import com.gitlab.candicey.astral.hook.NetworkManagerHook
import com.gitlab.candicey.astral.listener.GameOverlay
import com.gitlab.candicey.zenithcore.extension.registerHook
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.StartGameEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.font.FontManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.hook.LocaleHook
import com.gitlab.candicey.zenithcore.versioned.v1_8.keybind.KeyBind
import com.gitlab.candicey.zenithcore.versioned.v1_8.keybind.KeyBindManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import com.gitlab.candicey.zenithloader.ZenithLoader
import net.minecraft.client.gui.GuiChat
import net.weavemc.api.ModInitializer
import net.weavemc.api.event.EventBus
import net.weavemc.loader.InjectionHandler
import org.lwjgl.input.Keyboard
import java.awt.Font
import java.lang.instrument.Instrumentation

class AstralMain : ModInitializer {
    @Suppress("OVERRIDE_DEPRECATION")
    override fun preInit(inst: Instrumentation) {
        info("Initialising...")

        ZenithLoader.loadDependencies("astral", inst = inst)

        arrayOf(
            MinecraftHook,
            NetworkManagerHook,
            NetHandlerPlayClientHook,
            GuiMultiplayerHook,
        ).forEach(InjectionHandler::registerHook)

        arrayOf(
            GameOverlay,
        ).forEach(EventBus::subscribe)

        with(FontManager) {
            registerFont("Verdana", 16)
            registerFont("Verdana", 20, Font.BOLD)
            registerFont("Verdana", 24, Font.BOLD)
        }

        commandManager.registerCommand(
            ScreenshotCommand,
            BindToolCommand,
            RainbowSpamCommand,
            GamemodeCommand,
            ServerCommand,
            CrashCommand,
            ConfigCommand,
        )

        info("Initialised!")
    }

    override fun init() {
        info("Loading config...")
        configManager.init()
        info("Config loaded!")

        EventBus.subscribe(StartGameEvent.Post::class.java) {
            info("Initialising keybindings...")
            initKeybindings()
            info("Initialised keybindings!")
        }
    }

    private fun initKeybindings() {
        LocaleHook.messages.putAll(
            mapOf(
                "astral.overlay.toggle" to "Toggle Astral Game Overlay",
                "astral.command.open" to "Open Astral Command",
            )
        )

        KeyBindManager.registerKeyBindings(
            KeyBind("astral.overlay.toggle", Keyboard.KEY_NONE, "Astral") { _, _ ->
                GameOverlay.enabled = !GameOverlay.enabled
                "Astral Game Overlay has been ${if (GameOverlay.enabled) "${GREEN}enabled" else "${RED}disabled"}$YELLOW."
                    .addATPrefix().toChatComponent().addChatMessage()
            },
        )

        KeyBindManager.registerLateKeyBindings(
            KeyBind("astral.command.open", Keyboard.KEY_NONE, "Astral") { _, _ ->
                mc.displayGuiScreen(GuiChat("$PREFIX${COMMAND_PREFIX[0]} "))
            },
        )
    }
}