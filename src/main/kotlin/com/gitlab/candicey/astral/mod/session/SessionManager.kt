package com.gitlab.candicey.astral.mod.session

import com.gitlab.candicey.astral.configSession

object SessionManager {
    // Whether the user is premium, true by default (lunar client)
    var premium = true

    var usePremiumUuid: Boolean
        get() = configSession.config.usePremiumUuid
        set(value) =
            configSession
                .also { it.config.usePremiumUuid = value }
                .writeConfig()

    // If the user is not premium, this is the UUID of fake premium account
    var premiumUuid: String?
        get() = configSession.config.premiumUuid
        set(value) =
            configSession
                .also { it.config.premiumUuid = value }
                .writeConfig()

    var nick: String?
        get() = configSession.config.nick
        set(value) =
            configSession
                .also { it.config.nick = value }
                .writeConfig()

    var fakeNick: String?
        get() = configSession.config.fakeNick
        set(value) =
            configSession
                .also { it.config.fakeNick = value }
                .writeConfig()
}