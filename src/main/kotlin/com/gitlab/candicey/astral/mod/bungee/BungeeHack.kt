package com.gitlab.candicey.astral.mod.bungee

import com.gitlab.candicey.astral.configBungeeHack

object BungeeHack {
    var enabled: Boolean by configBungeeHack.delegateValue()

    var ip: String by configBungeeHack.delegateValue()
}