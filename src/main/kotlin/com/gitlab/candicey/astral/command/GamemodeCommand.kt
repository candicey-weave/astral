package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.world.WorldSettings.GameType

@CommandInfo("gamemode", "gm")
object GamemodeCommand : Command() {
    private var realGamemode: GameType? = null

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        val string = args[0].toLowerCase()

        if (string == "reset") {
            if (realGamemode == null) {
                "You haven't changed your gamemode yet!".addATPrefix().toChatComponent().addChatMessage()
            } else {
                mc.playerController.setGameType(realGamemode)
                "Reset your gamemode to $GREEN${realGamemode!!.name.toLowerCase()}$YELLOW.".addATPrefix().toChatComponent().addChatMessage()
                realGamemode = null
            }
            return
        }

        val gamemode = getGamemode(args[0])
            ?: run {
                "${RED}Invalid gamemode.".addATPrefix().toChatComponent().addChatMessage()
                return
            }

        realGamemode = mc.theWorld.worldInfo.gameType
        mc.playerController.setGameType(gamemode)
        "Set your gamemode to $GREEN${gamemode.name.toLowerCase()}$YELLOW.".addATPrefix().toChatComponent().addChatMessage()
    }

    private fun getGamemode(string: String): GameType? =
        when (string) {
            "survival", "s", "0" -> GameType.SURVIVAL
            "creative", "c", "1" -> GameType.CREATIVE
            "adventure", "a", "2" -> GameType.ADVENTURE
            "spectator", "sp", "3" -> GameType.SPECTATOR
            else -> null
        }
}