package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.resources.I18n
import net.minecraft.item.Item

@CommandInfo("bindTool", "bt", "b")
object BindToolCommand : Command() {
    val toolBinds = mutableMapOf<Item, String>()

    private var clear = false

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        val item = mc.thePlayer.inventory.getCurrentItem()

        if (args.isEmpty()) {
            if (item != null) {
                clear = false

                if (toolBinds.remove(item.item!!) != null) {
                    "Tool bind removed.".addATPrefix().toChatComponent().addChatMessage()
                } else {
                    "This tool is not bound.".addATPrefix().toChatComponent().addChatMessage()
                }
            } else {
                if (!clear) {
                    clear = true

                    "Please type the command again to clear all tool binds.".addATPrefix().toChatComponent().addChatMessage()
                } else {
                    clear = false

                    toolBinds.clear()

                    "All tool binds cleared.".addATPrefix().toChatComponent().addChatMessage()
                }
            }
        } else {
            clear = false

            if (item == null) {
                "${RED}You must hold a tool in your hand to bind it.".addATPrefix().toChatComponent().addChatMessage()
                return
            }

            val command = args.joinToString(" ")
            toolBinds[item.item!!] = command

            val itemName = I18n.format(item.item!!.unlocalizedName + ".name")
            val itemNameCapitalised = itemName.substring(0, 1).toUpperCase() + itemName.substring(1)
            "$itemNameCapitalised has been bound.".addATPrefix().toChatComponent().addChatMessage()
        }
    }
}