package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.command.config.ConfigReloadCommand
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo

@CommandInfo("config")
object ConfigCommand : Command() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            ConfigReloadCommand,
        )
}