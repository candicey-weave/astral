package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.commandInitialisationData
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract

abstract class Command : CommandAbstract(commandInitialisationData)