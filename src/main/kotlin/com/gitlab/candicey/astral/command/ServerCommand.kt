package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.command.server.ServerOnlineCommand
import com.gitlab.candicey.astral.command.server.ServerPluginsCommand
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc

@CommandInfo("server", "s")
object ServerCommand : Command() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            ServerOnlineCommand,
            ServerPluginsCommand,
        )

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        runSubCommand = !mc.isSingleplayer

        if (!runSubCommand) {
            "${RED}You must be on a server to use this command.".addATPrefix().toChatComponent().addChatMessage()
            return
        }

        checkArgs(args, previousCommandName)
    }
}