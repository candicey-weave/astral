package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.LOGGER
import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.extension.*
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.PacketEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.matches
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.util.sendPacket
import net.minecraft.network.play.client.C14PacketTabComplete
import net.minecraft.network.play.server.S3APacketTabComplete
import net.weavemc.api.event.SubscribeEvent

@CommandInfo("massScan", "ms")
object ServerPluginsMassScanCommand : Command() {
    private const val completionStarts = "/:abcdefghijklmnopqrstuvwxyz0123456789-"

    private var enabled = false

    private var ticks = 0

    private var plugins = mutableListOf<String>()

    init {
        subscribeEventListener()
    }

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        enabled = true
        ticks = 0

        "Plugins scan started. This may take around 5 seconds.".addATPrefix().toChatComponent().addChatMessage()

        runAsync {
            completionStarts.forEach { char ->
                sendPacket(C14PacketTabComplete(char.toString()))
                Thread.sleep(100)
            }
        }
    }

    @SubscribeEvent
    fun onTick(event: TickEvent.Pre) {
        if (!enabled) {
            return
        }

        if (++ticks >= 200) {
            ServerPluginsCommand.printPlugins(plugins)
            enabled = false
        }
    }

    @SubscribeEvent
    fun onPacketReceived(event: PacketEvent.Receive) {
        if (!enabled) {
            return
        }

        val packet = event.packet
        if (event.packet !is S3APacketTabComplete) {
            return
        }

        val packetTabComplete = packet as S3APacketTabComplete
        val matches = packetTabComplete.matches
            ?: run {
                LOGGER.error("PacketTabComplete matches is null")
                return
            }

        matches.forEach { string ->
            val command = string.split(":")

            if (command.size > 1) {
                val pluginName = command[0].delete("/")

                if (pluginName !in plugins) {
                    plugins.add(pluginName)
                }
            }
        }
    }
}