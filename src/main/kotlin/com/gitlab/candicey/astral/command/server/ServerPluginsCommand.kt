package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.extension.*
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent

/**
 * From <a href="https://github.com/MeteorDevelopment/meteor-client>Meteor Client</a>
 */
@CommandInfo("plugins", "plugin", "pl", "p")
object ServerPluginsCommand : Command() {
    private val anticheatList: List<String> = mutableListOf(
        "nocheatplus",
        "negativity",
        "warden",
        "horizon",
        "illegalstack",
        "coreprotect",
        "exploitsx",
        "vulcan",
        "abc",
        "spartan",
        "kauri",
        "anticheatreloaded",
        "witherac",
        "godseye",
        "matrix",
        "wraith",
    )

    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            ServerPluginsBukkitCommand,
            ServerPluginsMassScanCommand,
        )

    fun printPlugins(plugins: List<String>) {
        if (plugins.isEmpty()) {
            "No plugins found.".addATPrefix().toChatComponent().addChatMessage()
            return
        }

        val sorted = plugins.sorted().trim()

        val stringBuilder = StringBuilder("Plugins: ")

        sorted.forEach({ plugin ->
            if (anticheatList.contains(plugin.toLowerCase()) || plugin.contains("exploit", "cheat", "illegal", ignoreCase = true)) {
                stringBuilder.append("$RED$plugin")
            } else {
                stringBuilder.append("$GREEN$plugin")
            }
        }, init = {
            stringBuilder.append("$YELLOW, ")
        }, last =  {
            stringBuilder.append("$YELLOW.")
        })

        stringBuilder.toString().addATPrefix().toChatComponent().addChatMessage()
    }
}