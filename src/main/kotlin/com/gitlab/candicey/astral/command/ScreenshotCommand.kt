package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.configGameOverlay
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.astral.warn
import com.gitlab.candicey.zenithcore.extension.*
import com.gitlab.candicey.zenithcore.helper.ImgurHelper
import com.gitlab.candicey.zenithcore.util.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.CustomClickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithClickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithRunnable
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.ScreenshotHelper
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.settings.KeyBinding
import net.minecraft.event.ClickEvent
import net.minecraft.event.HoverEvent
import net.minecraft.util.IChatComponent
import net.weavemc.api.event.SubscribeEvent

@CommandInfo("screenshot", "ss", description = "Hides the server ip and makes the screenshot automatically.")
object ScreenshotCommand : Command() {
    var last: IChatComponent? = null

    var hideIp: Boolean
        get() = configGameOverlay.config.hideIp
        set(value) =
            configGameOverlay
                .also { it.config.hideIp = value }
                .writeConfig()

    var takingScreenshot = false

    private var ticks = 0

    init {
        subscribeEventListener()
    }

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            takingScreenshot = true
            KeyBinding.setKeyBindState(mc.gameSettings.keyBindScreenshot.keyCode, true)
            return
        }

        when (args[0]) {
            "on" -> {
                hideIp = true
                "Server IP hidden.".addATPrefix().toChatComponent().addChatMessage()
            }

            "off" -> {
                hideIp = false
                "Server IP shown.".addATPrefix().toChatComponent().addChatMessage()
            }

            "toggle" -> {
                hideIp = !hideIp
                "Server IP ${if (hideIp) "hidden" else "shown"}.".addATPrefix().toChatComponent().addChatMessage()
            }

            else -> {
                "Invalid argument.".addATPrefix().toChatComponent().addChatMessage()
            }
        }
    }

    @SubscribeEvent
    fun onTick(event: TickEvent.Post) {
        if (!takingScreenshot) {
            return
        }

        if (++ticks > 1) {

            takingScreenshot = false
            ticks = 0

            ScreenshotHelper.saveScreenshotToFile { success, file, throwable ->
                if (success) {
                    if (file == null) {
                        warn("Screenshot file is null.")
                        return@saveScreenshotToFile
                    } else {
                        val savedScreenshotMessage = "${YELLOW}Screenshot has been saved.".toChatComponent()
                        savedScreenshotMessage.chatStyle.apply {
                            underlined = true
                            chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, file.name.toChatComponent())
                        }
                        last = savedScreenshotMessage

                        val open = "$GOLD[Open]".toChatComponent()
                        open.chatStyle.apply {
                            chatClickEvent = ClickEvent(ClickEvent.Action.OPEN_FILE, file.absolutePath)
                            chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, "Open screenshot".toChatComponent())
                        }

                        val copy = "$BLUE[Copy]".toChatComponent()
                        copy.chatStyle.apply {
                            zenithClickEvent = ClickEvent(CustomClickEvent.COPY_IMAGE_TO_CLIPBOARD, file.absolutePath)
                                .apply {
                                    zenithRunnable = Runnable {
                                        "Screenshot has been copied to clipboard."
                                            .addATPrefix().toChatComponent().addChatMessage()
                                    }
                                }
                            chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, "Copy screenshot".toChatComponent())
                        }

                        val upload = "$GREEN[Upload]".toChatComponent()
                        upload.chatStyle.apply {
                            zenithClickEvent = ClickEvent(CustomClickEvent.NONE, null)
                                .apply {
                                    zenithRunnable = Runnable {
                                        runAsync {
                                            val imgurResponse = ImgurHelper.upload(file)

                                            if (imgurResponse.success) {
                                                val uploadedScreenshot = "Screenshot has been uploaded. ".addATPrefix().toChatComponent()
                                                val copyUrl = "$BLUE[Copy]".toChatComponent()
                                                copyUrl.chatStyle.apply {
                                                    bold = true
                                                    zenithClickEvent =
                                                        ClickEvent(CustomClickEvent.COPY_TEXT_TO_CLIPBOARD, imgurResponse.data.getUrl())
                                                            .apply {
                                                                zenithRunnable = Runnable {
                                                                    "Image url has been copied to clipboard."
                                                                        .addATPrefix().toChatComponent().addChatMessage()
                                                                }
                                                            }
                                                    chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, "Copy url".toChatComponent())

                                                    uploadedScreenshot.appendSibling(copyUrl).addChatMessage()
                                                }
                                            } else {
                                                "${RED}Failed to upload screenshot."
                                                    .addATPrefix().toChatComponent().addChatMessage()
                                                warn("Failed to upload screenshot: $imgurResponse")
                                            }
                                        }
                                    }
                                }
                            chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, "Upload screenshot to ${GREEN}imgur.com".toChatComponent())
                        }

                        val space = " ".toChatComponent()

                        "".addATPrefix()
                            .toChatComponent()
                            .appendSibling(savedScreenshotMessage)
                            .appendSibling(space)
                            .appendSibling(open)
                            .appendSibling(space)
                            .appendSibling(copy)
                            .appendSibling(space)
                            .appendSibling(upload)
                            .addChatMessage()
                    }
                } else {
                    warn("Failed to take screenshot: $throwable")
                }
            }
        }
    }
}