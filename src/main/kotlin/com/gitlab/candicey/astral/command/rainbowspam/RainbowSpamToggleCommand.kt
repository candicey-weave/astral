package com.gitlab.candicey.astral.command.rainbowspam

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.command.RainbowSpamCommand
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent

@CommandInfo("toggle", "t")
object RainbowSpamToggleCommand : Command() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        RainbowSpamCommand.enabled = !RainbowSpamCommand.enabled
        "RainbowSpam has been ${if (RainbowSpamCommand.enabled) "${GREEN}enabled" else "${RED}disabled"}$YELLOW."
            .addATPrefix()
            .toChatComponent()
            .addChatMessage()
    }
}