package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.extension.forEach
import com.gitlab.candicey.zenithcore.extension.trim
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent

@CommandInfo("online", "o")
object ServerOnlineCommand : Command() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            ServerOnlinePlayerDataCommand,
            ServerOnlineTabCompleteCommand,
        )

    fun printPlayerList(playerList: List<String>) {
        if (playerList.isEmpty()) {
            "No players found".addATPrefix().toChatComponent().addChatMessage()
            return
        }

        val sorted = playerList.sorted().trim()

        val stringBuilder = StringBuilder("Players: ")

        sorted.forEach({ player ->
            stringBuilder.append("$GREEN$player")
        }, init = {
            stringBuilder.append("$YELLOW, ")
        }, last =  {
            stringBuilder.append("$YELLOW.")
        })

        stringBuilder.toString().addATPrefix().toChatComponent().addChatMessage()
    }

    fun printPlayerCount(playerCount: Int) {
        when (playerCount) {
            0 -> "No players online".addATPrefix().toChatComponent().addChatMessage()
            1 -> "${GREEN}1 ${YELLOW}player online.".addATPrefix().toChatComponent().addChatMessage()
            else -> "$GREEN$playerCount ${YELLOW}players online.".addATPrefix().toChatComponent().addChatMessage()
        }
    }

    fun printInvalidArgument() {
        "Invalid argument. Valid arguments are list, count.".addATPrefix().toChatComponent().addChatMessage()
    }
}