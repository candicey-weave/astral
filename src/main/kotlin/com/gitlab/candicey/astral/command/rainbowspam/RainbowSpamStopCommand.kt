package com.gitlab.candicey.astral.command.rainbowspam

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.command.RainbowSpamCommand
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo

@CommandInfo("stop", "disable", "off")
object RainbowSpamStopCommand : Command() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        RainbowSpamCommand.enabled = false
    }
}