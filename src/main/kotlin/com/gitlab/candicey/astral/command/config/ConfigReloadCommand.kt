package com.gitlab.candicey.astral.command.config

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.configManager
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent

@CommandInfo("reload")
object ConfigReloadCommand : Command() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        configManager.init()
        "Config reloaded!".addATPrefix().toChatComponent().addChatMessage()
    }
}