package com.gitlab.candicey.astral.command.rainbowspam

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.gui.RainbowSpamGui
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.GuiScreenHelper

@CommandInfo("gui", "ui", "g", "settings", "setting", "st")
object RainbowSpamGuiCommand : Command() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        GuiScreenHelper.addGuiOpenQueue(RainbowSpamGui())
    }
}