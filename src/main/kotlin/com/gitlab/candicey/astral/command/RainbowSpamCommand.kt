package com.gitlab.candicey.astral.command

import com.gitlab.candicey.astral.command.rainbowspam.RainbowSpamGuiCommand
import com.gitlab.candicey.astral.command.rainbowspam.RainbowSpamStartCommand
import com.gitlab.candicey.astral.command.rainbowspam.RainbowSpamStopCommand
import com.gitlab.candicey.astral.command.rainbowspam.RainbowSpamToggleCommand
import com.gitlab.candicey.astral.configRainbowSpam
import com.gitlab.candicey.zenithcore.extension.subscribeEventListener
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.WorldEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.sendChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import net.weavemc.api.event.SubscribeEvent
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

@CommandInfo("rainbowSpam", "rs")
object RainbowSpamCommand : Command() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            RainbowSpamStartCommand,
            RainbowSpamStopCommand,
            RainbowSpamToggleCommand,
            RainbowSpamGuiCommand,
        )

    var enabled = false
        set(value) {
            field = value

            if (value) {
                scheduledThread = executors.scheduleAtFixedRate({
                    "${configRainbowSpam.config.command} &${nextRainbowChar()}${configRainbowSpam.config.rainbow}"
                        .toChatComponent()
                        .sendChatMessage()
                }, 0, configRainbowSpam.config.delay.toLong(), TimeUnit.MILLISECONDS)
            } else {
                scheduledThread?.cancel(true)
            }
        }

    private const val rainbowChars = "4c6e2ab319d5f780"
    private var rainbowIndex = 0

    private val executors = Executors.newScheduledThreadPool(2)

    private var scheduledThread: ScheduledFuture<*>? = null

    init {
        subscribeEventListener()
    }

    private fun nextRainbowChar(): Char {
        if (rainbowIndex >= rainbowChars.length) {
            rainbowIndex = 0
        }

        return rainbowChars[rainbowIndex++]
    }

    @SubscribeEvent
    fun onUnloadWorld(event: WorldEvent.Unload) {
        enabled = false
    }
}