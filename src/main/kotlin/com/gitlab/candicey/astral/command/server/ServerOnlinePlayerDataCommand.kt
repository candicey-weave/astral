package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.util.MinecraftUtil

@CommandInfo("playerData", "pd")
object ServerOnlinePlayerDataCommand : Command() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            ServerOnlineCommand.printInvalidArgument()
            return
        }

        val onlinePlayers by lazy { MinecraftUtil.getOnlinePlayers() }

        when (args[0].toLowerCase()) {
            "list" -> {
                ServerOnlineCommand.printPlayerList(onlinePlayers.map { it.name })
            }
            "count" -> {
                ServerOnlineCommand.printPlayerCount(onlinePlayers.size)
            }
            else -> {
                ServerOnlineCommand.printInvalidArgument()
            }
        }
    }
}