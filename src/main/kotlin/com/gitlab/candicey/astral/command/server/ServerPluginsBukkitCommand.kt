package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.LOGGER
import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.extension.subscribeEventListener
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.PacketEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.matches
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.util.sendPacket
import net.minecraft.network.play.client.C14PacketTabComplete
import net.minecraft.network.play.server.S3APacketTabComplete
import net.weavemc.api.event.SubscribeEvent

@CommandInfo("bukkit", "bkt", "bk", "b")
object ServerPluginsBukkitCommand : Command() {
    private var ticks = 0

    private var commandTypeEnum: BukkitPluginsType? = null

    init {
        subscribeEventListener()
    }

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        commandTypeEnum = let {
            if (args.isEmpty()) null
            else BukkitPluginsType.fromCommand(args[0])
        } ?: run {
                "${RED}Invalid mode. Valid modes are ${BukkitPluginsType.values().joinToString(", ") { it.command }}."
                    .addATPrefix().toChatComponent().addChatMessage()
                return
            }

        ticks = 0

        sendPacket(C14PacketTabComplete("/${commandTypeEnum!!.toCommand()} "))
    }

    @SubscribeEvent
    fun onTick(event: TickEvent.Pre) {
        if (commandTypeEnum == null) {
            return
        }

        if (++ticks >= 200) {
            "${RED}Plugins check timed out. Either the packet has been dropped, or you dont have access to the ${commandTypeEnum!!.toCommand()} command."
                .addATPrefix().toChatComponent().addChatMessage()
            commandTypeEnum = null
        }
    }

    @SubscribeEvent
    fun onPacketReceived(event: PacketEvent.Receive) {
        if (commandTypeEnum == null) {
            return
        }

        val packet = event.packet
        if (event.packet !is S3APacketTabComplete) {
            return
        }

        val packetTabComplete = packet as S3APacketTabComplete
        val matches = packetTabComplete.matches
            ?: run {
                LOGGER.error("PacketTabComplete matches is null")
                return
            }

        ServerPluginsCommand.printPlugins(matches.toList())
        commandTypeEnum = null
    }

    enum class BukkitPluginsType(
        val command: String,
    ) {
        VER("ver"),
        VERSION("version"),
        ABOUT("about"),
        QUESTION_MARK("?");

        companion object {
            fun fromCommand(command: String): BukkitPluginsType? = values().find { it.command == command }
        }

        fun toCommand(): String = "bukkit:$command"
    }
}