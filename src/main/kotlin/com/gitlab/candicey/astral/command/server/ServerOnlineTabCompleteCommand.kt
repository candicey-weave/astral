package com.gitlab.candicey.astral.command.server

import com.gitlab.candicey.astral.LOGGER
import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.extension.*
import com.gitlab.candicey.zenithcore.nameRegex
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInfo
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.PacketEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.matches
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.util.sendPacket
import net.minecraft.network.play.client.C14PacketTabComplete
import net.minecraft.network.play.server.S3APacketTabComplete
import net.weavemc.api.event.SubscribeEvent
import java.util.*

@CommandInfo("tabComplete", "tc")
object ServerOnlineTabCompleteCommand : Command() {
    private var enabled = false

    private var ticks = 0

    /**
     * @value false = player count
     * @value true = player list
     */
    private var listPlayer = true

    init {
        subscribeEventListener()
    }

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        ticks = 0
        enabled = true

        if (args.isEmpty()) {
            ServerOnlineCommand.printInvalidArgument()
            return
        }

        listPlayer = when (args[0].lowercase(Locale.getDefault())) {
            "list" -> true
            "count" -> false
            else -> {
                ServerOnlineCommand.printInvalidArgument()
                return
            }
        }

        sendPacket(C14PacketTabComplete(" "))
    }

    @SubscribeEvent
    fun onTick(event: TickEvent.Pre) {
        if (!enabled) {
            return
        }

        if (++ticks >= 200) {
            "${RED}Player list check timed out.".addATPrefix().toChatComponent().addChatMessage()
            enabled = false
        }
    }

    @SubscribeEvent
    fun onPacketReceived(event: PacketEvent.Receive) {
        if (!enabled) {
            return
        }

        val packet = event.packet
        if (event.packet !is S3APacketTabComplete) {
            return
        }

        val packetTabComplete = packet as S3APacketTabComplete
        val matches = packetTabComplete.matches
            ?: run {
                LOGGER.error("PacketTabComplete matches is null")
                return
            }

        val playerList = matches.toList().trim().filter { nameRegex.matches(it) }
        if (listPlayer) {
            ServerOnlineCommand.printPlayerList(playerList)
        } else {
            ServerOnlineCommand.printPlayerCount(playerList.size)
        }

        enabled = false
    }
}