package com.gitlab.candicey.astral.command.crash

import com.gitlab.candicey.astral.command.Command
import com.gitlab.candicey.astral.command.calculateMethodTime
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent

abstract class CrashEntry : Command() {
    abstract val methodName: String

    abstract fun crash()

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            "${RED}Please specify the packet count.".addATPrefix().toChatComponent().addChatMessage()
            return
        }

        val packetCount = args[0].toIntOrNull()
            ?.takeIf { it > 0 }
            ?: run {
                "${RED}Invalid packet count!".addATPrefix().toChatComponent().addChatMessage()
                return
            }

        calculateMethodTime(methodName, packetCount) {
            repeat(packetCount) {
                crash()
            }
        }
    }
}