package com.gitlab.candicey.astral

import com.gitlab.candicey.astral.config.BungeeHackJson
import com.gitlab.candicey.astral.config.GameOverlayJson
import com.gitlab.candicey.astral.config.RainbowSpamJson
import com.gitlab.candicey.astral.config.SessionJson
import com.gitlab.candicey.astral.extension.addATPrefix
import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandInitialisationData
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.font.FontManager
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.awt.Font

internal val LOGGER: Logger = LogManager.getLogger("Astral")

internal val COMMAND_PREFIX = listOf("astral")
internal const val PREFIX = "/"

internal val commandInitialisationData by lazy {
    CommandInitialisationData(
        COMMAND_PREFIX,
        PREFIX
    ) { it.addATPrefix() }
}
internal val commandManager by lazy { CommandManager(commandInitialisationData) }

internal val configManager by lazy {
    ConfigManager(
        mutableMapOf(
            "bungeehack" to Config(resolveConfigPath("bungee.json"), BungeeHackJson()),
            "session" to Config(resolveConfigPath("session.json"), SessionJson()),
            "gameoverlay" to Config(resolveConfigPath("gameoverlay.json"), GameOverlayJson()),
            "rainbowspam" to Config(resolveConfigPath("rainbowspam.json"), RainbowSpamJson()),
        )
    )
}

internal val configBungeeHack: Config<BungeeHackJson> by lazy { configManager.getConfig() ?: error("Cannot find BungeeHack config!") }

internal val configSession: Config<SessionJson> by lazy { configManager.getConfig() ?: error("Cannot find Session config!") }

internal val configGameOverlay: Config<GameOverlayJson> by lazy { configManager.getConfig() ?: error("Cannot find GameOverlay config!") }

internal val configRainbowSpam: Config<RainbowSpamJson> by lazy { configManager.getConfig() ?: error("Cannot find RainbowSpam config!") }

internal val rFont by lazy { FontManager.getFont("Verdana", 16) }
internal val bFont by lazy { FontManager.getFont("Verdana", 20, Font.BOLD) }
internal val gbFont by lazy { FontManager.getFont("Verdana", 24, Font.BOLD) }