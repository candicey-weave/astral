package com.gitlab.candicey.astral.helper

import kotlin.math.max

object TpsHelper {
    // in milliseconds
    var lastPacketTime = -1L

    var tps = -1.0
    val timeUpdateTimeList = mutableListOf<Long>()

    private var time = System.currentTimeMillis()

    fun updateTps() {
        timeUpdateTimeList.add(max(1000L, System.currentTimeMillis() - time))
        if (timeUpdateTimeList.size > 5) {
            timeUpdateTimeList.removeAt(0)
        }

        tps = 20000 / (timeUpdateTimeList.sum().toDouble() / timeUpdateTimeList.size)
        time = System.currentTimeMillis()
    }
}