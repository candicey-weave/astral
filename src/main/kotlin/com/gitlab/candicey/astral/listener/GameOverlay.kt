package com.gitlab.candicey.astral.listener

import com.gitlab.candicey.astral.bFont
import com.gitlab.candicey.astral.command.ScreenshotCommand
import com.gitlab.candicey.astral.configGameOverlay
import com.gitlab.candicey.astral.helper.TpsHelper
import com.gitlab.candicey.astral.rFont
import com.gitlab.candicey.zenithcore.util.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.RenderGameOverlayEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.Minecraft
import net.weavemc.api.event.SubscribeEvent

object GameOverlay {
    var enabled: Boolean
        get() = configGameOverlay.config.enabled
        set(value) =
            configGameOverlay
                .also { it.config.enabled = value }
                .writeConfig()

    var watermark = "Astral"

    @SubscribeEvent
    fun onRenderGameOverlay(event: RenderGameOverlayEvent.Post) {
        if (!enabled) {
            return
        }

        bFont.drawStringWithShadow("$watermark | ${Minecraft.getDebugFPS()} FPS", 6F, 6F, ColourUtil.rainbowColour(0.5))

        if (mc.isSingleplayer) {
            return
        }

        if (ScreenshotCommand.hideIp || ScreenshotCommand.takingScreenshot) {
            rFont.drawStringWithShadow("Server: ${RED}HIDDEN", 6F, 20F, -1)
        } else {
            rFont.drawStringWithShadow("Server: ${RED}${mc.currentServerData?.serverIP}", 6F, 20F, -1)
        }

        val clientBrand = mc.thePlayer.clientBrand
        runCatching {
            if (clientBrand != null) {
                val serverBrand =
                    if (clientBrand.contains("<- ")) "${clientBrand.split(" ")[0]} -> ${clientBrand.split("<- ")[1]}"
                    else clientBrand.split(" ")[0]

                rFont.drawStringWithShadow("Engine: $RED$serverBrand", 6F, 30F, -1)
            }
        }.onFailure {
            rFont.drawStringWithShadow("Engine: ${RED}INVALID", 6F, 30F, -1)
        }

        val lastPacketTime = System.currentTimeMillis() - TpsHelper.lastPacketTime
        if (TpsHelper.lastPacketTime != -1L) {
            val lastPacketTimeString = when {
                lastPacketTime < 1000L -> "$GREEN$lastPacketTime"
                lastPacketTime < 7000L -> "$YELLOW$lastPacketTime"
                lastPacketTime < 15000L -> "$GOLD$lastPacketTime"
                else -> "$RED$lastPacketTime"
            }
            rFont.drawStringWithShadow("Last packet: $RED${lastPacketTimeString}ms", 6F, 40F, -1)
        } else {
            rFont.drawStringWithShadow("Last packet: ${RED}INVALID", 6F, 40F, -1)
        }

        val tps = TpsHelper.tps
        if (tps != -1.0) {
            val tpsString = when {
                tps > 15.0 -> "$GREEN${String.format("%.2f", tps)}"
                tps > 10.0 -> "$YELLOW${String.format("%.2f", tps)}"
                tps > 5.0 -> "$GOLD${String.format("%.2f", tps)}"
                else -> "$RED${String.format("%.2f", tps)}"
            }
            rFont.drawStringWithShadow("PREDicted TPS: $RED$tpsString", 6F, 50F, -1)
        } else {
            rFont.drawStringWithShadow("PREDicted TPS: ${RED}INVALID", 6F, 50F, -1)
        }
    }
}