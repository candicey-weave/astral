package com.gitlab.candicey.astral.gui

import com.gitlab.candicey.astral.command.RainbowSpamCommand
import com.gitlab.candicey.astral.configRainbowSpam
import com.gitlab.candicey.astral.gbFont
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiScreenChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiTextFieldChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addButton
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.drawCenteredString
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.drawStringRightAligned
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiTextField

class RainbowSpamGui : GuiScreenChild() {
    val startY = 35

    var enabled = false

    private lateinit var command: GuiTextField

    private lateinit var rainbow: GuiTextField

    private lateinit var delay: GuiTextField

    private lateinit var toggleButton: GuiButton

    override fun initGui() {
        runCatching {
            enabled = RainbowSpamCommand.enabled

            command = GuiTextFieldChild(
                501,
                fontRendererObj,
                width / 2 - 100,
                startY,
                200,
                20
            ).apply {
                maxStringLength = 100
                text = configRainbowSpam.config.command
            }

            rainbow = GuiTextFieldChild(
                502,
                fontRendererObj,
                width / 2 - 100,
                startY + 50,
                200,
                20
            ).apply {
                maxStringLength = 100
                text = configRainbowSpam.config.rainbow
            }

            delay = GuiTextFieldChild(
                503,
                fontRendererObj,
                width / 2 + 3,
                startY + 75,
                50,
                20
            ).apply {
                maxStringLength = 8
                allowedCharactersMatcher = { char, _ -> char.isDigit() }
                text = configRainbowSpam.config.delay.toString()
            }

            toggleButton = GuiButton(
                401,
                width / 2 - 100,
                startY + 100,
                200,
                20,
                "Toggle: ${if (enabled) "${GREEN}enabled" else "${RED}disabled"}"
            )

            addGuiTextField(
                command,
                rainbow,
                delay,
            )

            addButton(
                toggleButton,
            )
        }.onFailure { it.printStackTrace() }
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        runCatching {
            drawRect(0, 0, width, height, 0xAA000000.toInt())
            super.drawScreen(mouseX, mouseY, partialTicks)

            gbFont.drawCenteredString("Rainbow Spammer", width / 2, 20, 0xFFFFFFFF.toInt())

            val offsetY = 5.5F
            drawStringRightAligned("Delay", (width / 2 - 5).toFloat(), startY + 75 + offsetY, 0xFFFFFFFF.toInt())
        }.onFailure { it.printStackTrace() }
    }

    override fun actionPerformed(button: GuiButton?) {
        if (button == null) {
            return
        }

        when (button.id) {
            401 -> {
                enabled = !enabled
                button.displayString = "Toggle: ${if (enabled) "${GREEN}enabled" else "${RED}disabled"}"
            }
        }

        super.actionPerformed(button)
    }

    override fun onGuiClosed() {
        val commandText = command.text.trim()
        val rainbowText = rainbow.text.trim()
        val delayText = delay.text.trim()

        val enabledCheck = enabled == RainbowSpamCommand.enabled
        val commandCheck = commandText == configRainbowSpam.config.command
        val rainbowCheck = rainbowText == configRainbowSpam.config.rainbow
        val delayCheck = delayText == configRainbowSpam.config.delay.toString()

        if (enabledCheck && commandCheck && rainbowCheck && delayCheck) {
            return
        }

        if (!enabledCheck) {
            RainbowSpamCommand.enabled = enabled
        }
        if (!commandCheck) {
            configRainbowSpam.config.command = commandText
        }
        if (!rainbowCheck) {
            configRainbowSpam.config.rainbow = rainbowText
        }
        if (!delayCheck) {
            delayText
                .toIntOrNull()
                ?.let {
                    configRainbowSpam.config.delay = if (it < 0) 100 else it
                }
        }

        configRainbowSpam.writeConfig()
    }
}