package com.gitlab.candicey.astral.gui

import com.gitlab.candicey.astral.fetchUUID
import com.gitlab.candicey.astral.mod.session.SessionManager
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiScreenChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiTextFieldChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addButton
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.drawCenteredString
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.setSession
import com.gitlab.candicey.zenithcore.versioned.v1_8.fontRenderer
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.GuiTextField
import net.minecraft.util.Session
import org.lwjgl.input.Keyboard
import java.awt.SystemColor.text

class AltManagerGui(private val previousScreen: GuiScreen) : GuiScreenChild() {
    private lateinit var premiumUuidButton: GuiButton
    private lateinit var loginButton: GuiButton

    private lateinit var nickTextField: GuiTextField
    private lateinit var fakeNickTextField: GuiTextField

    private var textString = ""
    private var textTick = 0

    private val premiumUuidButtonText: String
        get() = "Premium UUID: ${if (SessionManager.usePremiumUuid) "${GREEN}ON" else "${RED}OFF"}"

    override fun initGui() {
        premiumUuidButton = GuiButton(
            401,
            width / 2 - 100,
            height / 2 + 30,
            200,
            20,
            premiumUuidButtonText
        )
        loginButton = GuiButton(
            402,
            width / 2 - 100,
            height / 2 + 55,
            200,
            20,
            "Login"
        )

        nickTextField = GuiTextFieldChild(
            501,
            fontRenderer,
            width / 2 - 100,
            height / 2 - 50,
            200,
            20
        ).apply { SessionManager.nick?.let { text = it } }
        fakeNickTextField = GuiTextFieldChild(
            502,
            fontRenderer,
            width / 2 - 100,
            height / 2 - 5,
            200,
            20
        ).apply { SessionManager.fakeNick?.let { text = it } }

        addButton(
            premiumUuidButton,
            loginButton,
        )

        addGuiTextField(
            nickTextField,
            fakeNickTextField,
        )
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        drawBackground(0)

        super.drawScreen(mouseX, mouseY, partialTicks)

        fontRendererObj.drawCenteredString("Nick", width / 2, height / 2 - 65, 0xFFFFFFFF.toInt())
        fontRendererObj.drawCenteredString("Fake Nick", width / 2, height / 2 - 20, 0xFFFFFFFF.toInt())

        if (textTick-- > 0) {
            fontRendererObj.drawCenteredString(textString, width / 2, height / 2 + 70, 0xFFFFFFFF.toInt())
        }
    }

    override fun actionPerformed(button: GuiButton?) {
        val nick = nickTextField.text
        val fakeNick = fakeNickTextField.text

        when(button?.id) {
            401 -> {
                SessionManager.usePremiumUuid = !SessionManager.usePremiumUuid
                premiumUuidButton.displayString = premiumUuidButtonText
            }

            402 -> {
                if (SessionManager.usePremiumUuid) {
                    runCatching{ SessionManager.premiumUuid = fetchUUID(fakeNick) }

                    val session = mc.session
                    mc.setSession(Session(nick, session.playerID, session.token, (session.sessionType ?: Session.Type.MOJANG).name))
                    SessionManager.nick = nick
                    SessionManager.fakeNick = fakeNick
                    SessionManager.premium = false
                } else {
                    val session = mc.session
                    mc.setSession(Session(nick, session.playerID, session.token, (session.sessionType ?: Session.Type.MOJANG).name))
                    SessionManager.nick = nick
                    SessionManager.fakeNick = fakeNick
                }
            }
        }
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            mc.displayGuiScreen(previousScreen)
            return
        }

        super.keyTyped(char, keyCode)
    }

    private fun setText(text: String) {
        this.textString = text
        textTick = 3 * 20
    }
}