package com.gitlab.candicey.astral

import com.gitlab.candicey.zenithcore.JSON_PARSER
import java.io.File
import java.io.InputStreamReader
import java.net.URL

internal fun info(message: String) = LOGGER.info("[Astral] $message")
internal fun warn(message: String) = LOGGER.warn("[Astral] $message")

internal fun resolveConfigPath(fileName: String): File {
    val directory = File(System.getProperty("user.home"), ".weave/Astral")
    if (!directory.exists()) {
        directory.mkdirs()
    }
    return File(directory, fileName)
}

fun fetchUUID(username: String): String? =
    (JSON_PARSER.parse(InputStreamReader(URL("https://api.mojang.com/users/profiles/minecraft/$username").openStream()))).asJsonObject["id"].asString