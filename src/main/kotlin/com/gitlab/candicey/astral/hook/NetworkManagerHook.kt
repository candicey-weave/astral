package com.gitlab.candicey.astral.hook

import com.gitlab.candicey.astral.helper.TpsHelper
import com.gitlab.candicey.astral.mod.bungee.BungeeHack
import com.gitlab.candicey.astral.mod.session.SessionManager
import com.gitlab.candicey.zenithcore.extension.delete
import com.gitlab.candicey.zenithcore.util.callStatics
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.channel
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.ip
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import io.netty.channel.ChannelHandlerContext
import io.netty.util.concurrent.Future
import io.netty.util.concurrent.GenericFutureListener
import net.minecraft.network.EnumConnectionState
import net.minecraft.network.NetworkManager
import net.minecraft.network.Packet
import net.minecraft.network.handshake.client.C00Handshake
import net.minecraft.util.IChatComponent
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode
import java.util.*

object NetworkManagerHook : Hook("net/minecraft/network/NetworkManager") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStatics<NetworkManagerHook>(
            "channelRead0",
            "closeChannel",
            "dispatchPacket",
        )
    }

    @JvmStatic
    fun onChannelRead0(networkManager: NetworkManager, channelHandlerContext: ChannelHandlerContext?, packet: Packet<*>?) {
        if (networkManager.channel?.isOpen == true) {
            TpsHelper.lastPacketTime = System.currentTimeMillis()
        }
    }

    @JvmStatic
    fun onCloseChannel(networkManager: NetworkManager, message: IChatComponent) {
        TpsHelper.lastPacketTime = -1L
        TpsHelper.tps = -1.0
        TpsHelper.timeUpdateTimeList.clear()
    }

    @JvmStatic
    fun onDispatchPacket(networkManager: NetworkManager, packet: Packet<*>, futureListeners: Array<GenericFutureListener<out Future<in Void?>?>>?) {
        if (packet is C00Handshake) {
            if (BungeeHack.enabled && packet.requestedState == EnumConnectionState.LOGIN) {
                if (SessionManager.premium) {
                    packet.ip = "${packet.ip}\u0000${BungeeHack.ip}\u0000${mc.session.profile.id}"
                } else {
                    val premiumUuid = SessionManager.premiumUuid
                    if (premiumUuid == null) {
                        packet.ip = "${packet.ip}\u0000${BungeeHack.ip}\u0000${
                            UUID.nameUUIDFromBytes(("OfflinePlayer:${mc.session.profile.name}").toByteArray())
                                .toString().delete("-".toRegex())
                        }"
                    } else {
                        packet.ip = "${packet.ip}\u0000${BungeeHack.ip}\u0000$premiumUuid"
                    }
                }
            }
        }
    }
}