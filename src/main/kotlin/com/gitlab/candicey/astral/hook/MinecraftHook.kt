package com.gitlab.candicey.astral.hook

import com.gitlab.candicey.astral.command.BindToolCommand
import com.gitlab.candicey.zenithcore.util.callStatics
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.sendChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import net.minecraft.client.Minecraft
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode

object MinecraftHook : Hook("net/minecraft/client/Minecraft") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStatics<MinecraftHook>(
            "rightClickMouse",
        )
    }

    @JvmStatic
    fun onRightClickMouse(mc: Minecraft) {
        if (mc.playerController.isHittingBlock) {
            return
        }

        if (mc.objectMouseOver == null) {
            return
        }

        val currentItem = mc.thePlayer.inventory.getCurrentItem() ?: return
        val boundMessage = BindToolCommand.toolBinds[currentItem.item] ?: return

        boundMessage.toChatComponent().sendChatMessage()
    }
}