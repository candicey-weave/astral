package com.gitlab.candicey.astral.hook

import com.gitlab.candicey.astral.gui.AltManagerGui
import com.gitlab.candicey.astral.mod.bungee.BungeeHack
import com.gitlab.candicey.zenithcore.enum.InjectPosition
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.callStatics
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiTextFieldChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addButton
import com.gitlab.candicey.zenithcore.versioned.v1_8.fontRenderer
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiMultiplayer
import net.minecraft.client.gui.GuiTextField
import net.weavemc.api.Hook
import org.lwjgl.input.Keyboard
import org.objectweb.asm.tree.ClassNode

object GuiMultiplayerHook : Hook("net/minecraft/client/gui/GuiMultiplayer") {
    private lateinit var bungeeHackButton: GuiButton
    private lateinit var bungeeHackTextField: GuiTextField

    private lateinit var uuidSpoofButton: GuiButton

    private val bungeeHackButtonText: String
        get() = "Bungee: ${if (BungeeHack.enabled) "${GREEN}ON" else "${RED}OFF"}"

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStatics<GuiMultiplayerHook>(
            "onGuiClosed",
            "createButtons",
            "actionPerformed",
            "mouseClicked",
            "keyTyped",
        )

        node.callStatics<GuiMultiplayerHook>(
            "drawScreen",
            at = InjectPosition.RETURN
        )
    }

    @JvmStatic
    fun onGuiClosed(guiMultiplayer: GuiMultiplayer) {
        BungeeHack.ip = bungeeHackTextField.text
    }

    @JvmStatic
    fun onCreateButtons(guiMultiplayer: GuiMultiplayer) {
        guiMultiplayer.run {
            bungeeHackButton = GuiButton(
                401,
                width / 2 - 260,
                height - 52,
                100,
                20,
                bungeeHackButtonText
            )
            uuidSpoofButton = GuiButton(
                402,
                width / 2 + 160,
                height - 52,
                100,
                20,
                "UUID Spoof"
            )

            bungeeHackTextField = GuiTextFieldChild(
                501,
                fontRenderer,
                width / 2 - 258,
                height - 26,
                96,
                16
            ).apply { text = BungeeHack.ip }

            addButton(
                bungeeHackButton,
                uuidSpoofButton,
            )
        }
    }

    @JvmStatic
    fun onDrawScreen(guiMultiplayer: GuiMultiplayer, mouseX: Int, mouseY: Int, partialTicks: Float) {
        guiMultiplayer.run {
            bungeeHackTextField.drawTextBox()
        }
    }

    @JvmStatic
    fun onActionPerformed(guiMultiplayer: GuiMultiplayer, guiButton: GuiButton?) {
        guiMultiplayer.run {
            when (guiButton?.id) {
                401 -> {
                    BungeeHack.enabled = !BungeeHack.enabled
                    guiButton.displayString = bungeeHackButtonText
                }

                402 -> {
                    mc.displayGuiScreen(AltManagerGui(guiMultiplayer))
                }
            }
        }
    }

    @JvmStatic
    fun onMouseClicked(guiMultiplayer: GuiMultiplayer, mouseX: Int, mouseY: Int, mouseButton: Int) {
        guiMultiplayer.run {
            bungeeHackTextField.mouseClicked(mouseX, mouseY, mouseButton)
        }
    }

    @JvmStatic
    fun onKeyTyped(guiMultiplayer: GuiMultiplayer, typedChar: Char, keyCode: Int) {
        guiMultiplayer.run {
            if (bungeeHackTextField.isFocused) {
                if (keyCode == Keyboard.KEY_TAB) {
                    bungeeHackTextField.isFocused = false
                } else {
                    bungeeHackTextField.textboxKeyTyped(typedChar, keyCode)
                }
            }
        }
    }
}