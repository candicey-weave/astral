package com.gitlab.candicey.astral.hook

import com.gitlab.candicey.astral.helper.TpsHelper
import com.gitlab.candicey.zenithcore.enum.InjectPosition
import com.gitlab.candicey.zenithcore.util.callStatic
import net.minecraft.client.network.NetHandlerPlayClient
import net.minecraft.network.play.server.S03PacketTimeUpdate
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode

object NetHandlerPlayClientHook : Hook("net/minecraft/client/network/NetHandlerPlayClient") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val targetMethod = node.methods.run {
            find { it.name == "handleTimeUpdate" }
                ?: first { it.desc == "(Lnet/minecraft/network/play/server/S03PacketTimeUpdate;)V" }
        }

        node.callStatic<NetHandlerPlayClientHook>(
            callMethodName = ::onHandleTimeUpdate.name,
            injectMethodName = targetMethod.name,
            at = InjectPosition.RETURN
        )
    }

    @JvmStatic
    fun onHandleTimeUpdate(netHandlerPlayClient: NetHandlerPlayClient, packetIn: S03PacketTimeUpdate) {
        TpsHelper.updateTps()
    }
}