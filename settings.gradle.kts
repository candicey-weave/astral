pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/57327439/packages/maven") // Stellar
        maven("https://gitlab.com/api/v4/projects/57325214/packages/maven") // Weave Gradle
        maven("https://repo.weavemc.dev/releases")
    }
}

rootProject.name = "Astral"