# Astral
**Astral** is a [Weave](https://github.com/Weave-MC) mod for penetrating Minecraft servers.

<br>

## Features
**BungeeCord IP-Forwarding Bypass**
* Astral can bypass BungeeCord's IP-Forwarding feature, allowing you to connect to a server without having to go through a BungeeCord proxy.

**IP Spoofing**
* Astral can spoof your IP address, allowing you to connect to a server with a different IP address than your own.

**Username & UUID Spoofing**
* Astral can spoof your username and UUID, allowing you to connect to a server with a different username and UUID than your own.

**Server Info**
* Astral can get information about the server you are connected to, such as the server's plugins, online players.

**Server Crasher**
* Astral can crash servers by sending crashing packets to the server.

**Rainbow Spammer**
* Astral can spam the chat with rainbow text.

**Tool Binding**
* Astral can bind chat messages to tools, allowing you to send chat messages by right-clicking with a tool.

<br>

## Installation
To install Astral, follow the steps below:

1. Download the [Astral mod](#download).
2. Place the `.jar` file into your Weave mods directory:
    - **Windows**: `%userprofile%\.weave\mods` or `%userprofile%\.weave\mods\1.8.9`
    - **Unix**: `~/.weave/mods` or `~/.weave/mods/1.8.9`

<br>

## Build Instructions
To build Astral from source:

1. Clone the repository to your local machine.
2. In the root directory of the repository, run the following command:

   ```
   ./gradlew cleanBuild
   ```

3. After the build process completes, the compiled `.jar` files will be located in the `build/libs` directory.

<br>

## Download
The latest version of the mod can be downloaded from the [Package Registry](https://gitlab.com/candicey-weave/astral/-/packages). Select the most recent version and download the `.jar` file that ends with `-relocated.jar`.

<br>

## License
- Astral is licensed under the [GNU General Public License Version 3](https://gitlab.com/candicey-weave/astral/-/blob/master/LICENSE).