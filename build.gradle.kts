plugins {
    kotlin("jvm") version "1.9.23"
    `maven-publish`
    id("com.gitlab.candicey.stellar") version "0.2.1"
    id("net.weavemc.gradle") version "1.0.0-PRE"
}

val projectName: String by project
val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

weave {
    configure {
        name = projectName
        modId = projectGroup
        entryPoints = listOf("$projectGroup.AstralMain")
        mixinConfigs = listOf()
        mcpMappings()
    }
    version("1.8.9")
}

stellar {
    relocate {
        addStringReplacement("@@ASTRAL_VERSION@@", projectVersion)
    }

    dependencies {
        zenithCore version "2.0.2"
        zenithCoreVersionedApi version "v1_8"
        zenithLoader version "0.3.5"
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    maven("https://repo.weavemc.dev/releases")
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("net.weavemc:loader:1.0.0-b.2")
    implementation("net.weavemc:internals:1.0.0-b.2")
    implementation("net.weavemc.api:common:1.0.0-b.2")
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])

            val libsDirectory = File(project.projectDir, "build/libs")
            val file = libsDirectory.resolve("${project.name}-${project.version}-relocated.jar")
            artifact(file) {
                classifier = "relocated"
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

kotlin {
    jvmToolchain(8)
}

java {
    withSourcesJar()
    withJavadocJar()
}